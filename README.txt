Better Forms 
------------

Better forms is a small module that allows you to customize CCK forms.
It is designed for basic user who would like to do things like:

* Redirect a form to a View, Node or a specific content type
* Change name of submit button
* Hide: body field, Publishing options, book, revision information,
  menu setting and URL path settings from the form display
* Selectively hide either the Preview button or the Delete button (or both)


Installation
------------
Install and enable the module as usual (typically into sites/all/modules).
Configure Better Forms options from the following path:
Site Configuration --> Better Forms


Permissions
-----------
Please grant permission only to system administrator accounts.
User Management --> Permissions --> Better Forms --> Administer Better Forms. 
(Flush menu cache after submitting permissions.)


Credits
-------

Providers Of Educational Technology Support (POETS), Graduate College of 
Education, San Francisco State University.

* Jonathan Foerster
* Carlos Romero-Julio
* Robert Demallac-Sauzier 

Maintainer (CRJ)
