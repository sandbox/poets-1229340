<?php

/**
 * @file
 * Consulting a form to lookup the values we need to modify.
 *
 * @param $form
 *   A reference to the form to be altered.
 * @param $flg_mode
 *   Substitute for $op
 * @param $flg_default
 *   Populates default values from the form
 */

/**
 * Return better_formstemplate with values.
 */
function better_forms_rendertemplate() {
  // Create better_formstemplate.
  return drupal_get_form("better_formstemplate");
}

/**
 * Return array better_formstemplate (below all the template field).
 */
function better_formstemplate($form_state = array()) {
  $form = array();

  // Form mode display.
  $flg_mode = '';
  $flg_default = array();
  if (arg(5) == 'new') {
    $flg_mode = t('Add Form');
  }
  elseif (arg(4) == 'edit') {
    $flg_mode = t('Edit Form');
    $flg_default = better_forms_populate(arg(5), $flg_default);
  }

  $form['entry'] = array(
    '#type' => 'fieldset',
    '#title' => $flg_mode,
  );

  // Form Fields.
  $form['entry']['form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Form Title'),
    '#description' => t('Please enter your Form Title:'),
    '#size' => 100,
    '#default_value' => $flg_default['form_title'],
    '#requiered' => TRUE,
  );

  $form['entry']['form_name'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Content Type'),
    '#default_value' => $flg_default['form_name'],
    '#options' => better_forms_form_name($flg_default['form_name']),
    '#description' => t('Please select available Form Name (Form ID)'),
    '#requiered' => TRUE,
    '#size' => 1,
  );

  $form['entry']['redirect_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect Path'),
    '#default_value' => $flg_default['redirect_path'],
    '#description' => t('Please enter your Redirect Path (URL):'),
    '#size' => 100,
  );

  $form['entry']['submit_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit Button Text'),
    '#default_value' => $flg_default['submit_button'],
    '#description' => t('Please enter your Submit Button Text to replace:'),
    '#size' => 50,
  );

  $form['entry']['hide_fields'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Select field(s) to hide'),
    '#default_value' => explode(",", $flg_default['hide_fields']),
    '#options' => array(
      'body_field' => 'body_field',
      'options' => 'options',
      'revision_information' => 'revision_information',
      'path' => 'path',
      'menu' => 'menu',
      'book' => 'book',
      'comment_settings' => 'comment_settings',
    ),
    '#size' => 5,
  );

  $form['entry']['hide_preview_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide preview button'),
    '#default_value' => $flg_default['hide_preview_button'],
    '#size' => 50,
  );

  $form['entry']['hide_delete_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide delete button'),
    '#default_value' => $flg_default['hide_delete_button'],
    '#size' => 50,
  );

  $form['entry']['inline_fields'] = array(
    '#type' => 'checkbox',
    '#default_value' => $flg_default['inline_fields'],
    '#title' => t('Display all the form fields inline'),
    '#size' => 50,
  );

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Form'),
    '#validate' => array('better_forms_validate'),
    '#submit' => array('better_forms_submit'),
  );

  // Submit Cancel button.
  $form['backform'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('better_forms_backform'),
  );
  return $form;
}

/**
 * Canceling form.
 */
function better_forms_backform($form_id, &$form_state) {
  // Back to main form.
  $form_state['redirect'] = 'admin/settings/better_forms';
}

/**
 * Implements Validation before form is saved.
 */
function better_forms_validate($form_id, &$form_state) {

  if (empty($form_state['values']['form_title'])) {
    // Set error.
    form_set_error('Form title', t('Form Title can not be empty'));
    return;
  }

  if (empty($form_state['values']['form_name'])) {
    // Set error.
    form_set_error('Form Name', t('Content Type (Form ID) can not be empty'));
    return;
  }

}

/**
 * Populating form.
 */
function better_forms_populate($formid, $flg_populate = array()) {
  $flg_sql = "SELECT * FROM {better_forms} WHERE form_name = '%s'";
  $results  = db_query($flg_sql, $formid);

  // Get the next results as an associative array.
  while ($fields = db_fetch_array($results)) {
    // Iterate over all of the fields.
    foreach ($fields as $key => $flg_value) {
      $flg_populate[$key] = $flg_value;
    }
  }
  return $flg_populate;
}

/**
 * Populating form_name field.
 */
function better_forms_form_name($flg_value) {
  $flg_form_name = array();
  $flg_result = db_query('SELECT name, type FROM {node_type} WHERE type NOT IN (SELECT SUBSTRING(form_name,1,LENGTH(form_name)-10) as b_type FROM {better_forms})');

  // Get the next $flg_result as an associative array.
  while ($fields = db_fetch_array($flg_result)) {
    $key = t('@key_node_form', array('@key' => $fields['type']));
    $flg_form_name[$key] = $fields['type'];
  }

  $flg_form_name[$flg_value] = drupal_substr($flg_value, 0, drupal_strlen($flg_value) - 10);

  if (arg(5) == 'new') {
    $flg_form_name[''] = '- None -';
  }

  return $flg_form_name;
}

/**
 * Implements hook_submit().
 */
function better_forms_submit($form_id, &$form_state) {
  if (arg(5) == 'new') {
    $flg_data[1] = $form_state['values']['form_title'];
    $flg_data[2] = $form_state['values']['form_name'];
    $flg_data[3] = $form_state['values']['redirect_path'];
    $flg_data[4] = $form_state['values']['submit_button'];
    $flg_data[5] = $form_state['values']['hide_preview_button'];
    $flg_data[6] = $form_state['values']['hide_delete_button'];
    $flg_data[7] = $form_state['values']['inline_fields'];
    $flg_data[8] = implode(",", $form_state['values']['hide_fields']);
    db_query("INSERT INTO {better_forms} (form_title, form_name, redirect_path, submit_button, hide_preview_button, hide_delete_button, inline_fields, hide_fields) VALUES ('%s', '%s', '%s', '%s', %d, %d, %d,'%s')", $flg_data);
    $flg_message = t('Form <strong> %added </strong> has been Added', array('%added' => $flg_data[2]));
    drupal_set_message($flg_message);
  }
  elseif (arg(4) == 'edit') {
    $flg_data[1] = $form_state['values']['form_title'];
    $flg_data[2] = $form_state['values']['form_name'];
    $flg_data[3] = $form_state['values']['redirect_path'];
    $flg_data[4] = $form_state['values']['submit_button'];
    $flg_data[5] = $form_state['values']['hide_preview_button'];
    $flg_data[6] = $form_state['values']['hide_delete_button'];
    $flg_data[7] = $form_state['values']['inline_fields'];
    $flg_data[8] = implode(",", $form_state['values']['hide_fields']);
    $flg_data[9] = arg(5);
    db_query("UPDATE {better_forms} SET form_title='%s', form_name='%s', redirect_path='%s', submit_button='%s', hide_preview_button=%d, hide_delete_button=%d, inline_fields=%d, hide_fields='%s' WHERE form_name = '%s'", $flg_data);
    $flg_message = t('Form <strong> %updated </strong> has been updated', array('%updated' => arg(5)));
    drupal_set_message($flg_message);
  }

  $form_state['redirect'] = 'admin/settings/better_forms';
}

/**
 * Menu callback - ask for confirmation of form deletion.
 */
function better_forms_delete_confirm(&$form_state) {
  $form = array();
  $form['formid'] = array(
    '#type' => 'value',
    '#value' => arg(4),
  );

  $result = confirm_form($form,
    t('Delete form %form?', array('%form' => arg(4))),
    'admin/settings/better_forms',
    t('Are you sure you want to delete <strong> %form </strong>? This action cannot be undone.', array('%form' => arg(4))),
    t('Delete'), t('Cancel'),
    'confirm'
  );

  return $result;
}

/**
 * Execute form deletion.
 */
function better_forms_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $formid = $form_state['values']['formid'];

    if (!empty($formid)) {
      // Delete form.
      db_query("DELETE FROM {better_forms} WHERE form_name = '%s'", $formid);
      drupal_set_message(t('Form <strong> %form </strong> has been deleted.', array('%form' => $formid)));
    }

  }
  $form_state['redirect'] = 'admin/settings/better_forms';
}
